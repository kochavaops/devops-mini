# Kochava DevOps Mini Project

Welcome to the Kochava devops miniproject.  To help us assess your capabilities, we have a short hands-on project for you to complete.  This is not a timed project, so just keep us informed of your progress.  For time management purposes, we expect that the overall time for completion will be 4-6 hours.

## Your mission if you choose to accept it

You’ve just been hired by Company X and have been asked to build deploy a new API for monitoring the state of the infrastructure.  This new API is called FBAAS or Fizzbuzz As A Service.  The services that rely on this API expect consistent results otherwise the infrastructure (and the company) will burn to the ground in a glorious flash of light.  As such, not only does the return value need to be accurate, the instance must be tuned for optimal performance.

After the project has been completed it will be reviewed by multiple members of the DevOps team.  As such please provide enough documentation in the form of a README file and comments to help us understand specific decisions, the build process, deployment process and the test results.  It does not need to be exhastive, so don't go overboard, just give us the minimal information that you think we will need to properly evaluate.  If you are selected to move on to the on-site interviews, you will be presenting your project and fielding questions.

Once you have completed the project email Rob - rlyon@kochava.com and we will begin the review.

#### Setup

1. Create an account at [gitlab.com](https://gitlab.com) if you do not already have one and then create a new private repository to store your work.  Share this private repository with `@kochavaops` so we can follow your progress.
2. Your welcome email should have contained login information for a Digital Ocean dropplet that you will use to deploy your application.  Make sure that it is available and that you can log in.

#### Create and build the FBAAS application (~1 hour)

Using Go or Python, build the app with the endpoints defined in [FBAAS.md](FBAAS.md).  Try to use as few external packages/libraries as possible.  Make sure that you are adding appropriate testing for the application.

#### Deployment (~2 hours)

Once you have a working FBAAS api, deploy it to the VM that has been provided. You may use any methods to deploy and run the service. Just make sure that it is repeatable and documented.  Add the deployment scripts, playbooks, pillars, recipes, modules or manifests to your repository.  Your deployment method is entirely up to you - just be sure to document the deployment steps and stages.  Things you should consider while creating the deployment scripts or configurations:

1. OS level tuning.  The VM that you have been given is intentionally small and will require at least minimal tuning to the network stack.
2. Make sure that the application will start on boot.
3. A reverse proxy is allowed to help buffer requests, but caching must be disabled since it masks the effects of OS tuning during the next stress testing phase.
4. Everything that is done to the host must be included in the deployment.

#### Stress Test (~2 hours)

Create a free account at [Loader.io](https://loader.io) and benchmark the performance of your application.  Use the **Maintain Client Load** test and ramp up from 0 to 10,000 clients to your `/fizzbuzz/<num>` endpoint.   You will be competing with yourself here, so work at getting the best performance that you can get with minimal errors.  Make any adjustments that you need and once you have your application's performance in an optimal state, add the results and boundaries to your documentation.
