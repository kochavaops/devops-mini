# FBAAS API Endpoints

### GET /fizz/`<num>`

Returns the string value `Fizz` if `<num>` is divisible by 3.  The only numbers that are acceptable are 1-500. Numbers outside of those bounds are rejected. The expected states with the return code and JSON responses are as follows:

| Code | State                         | JSON Response              |
|:----:|-------------------------------|----------------------------|
| 200  | Number is divisible by 3      | {"value": "Fizz"}          |
| 200  | Number is not divisible by 3. | {"value": `<num>`}         |
| 400  | Number is out of bounds.      | {"error": "Out of bounds"} |

### GET /buzz/`<num>`

Returns the string value `Buzz` if `<num>` is divisible by 5.  The only numbers that are acceptable are 1-500. Numbers outside of those bounds are rejected. The expected states with the return code and JSON responses are as follows::

| Code | State                         | JSON Response              |
|:----:|-------------------------------|----------------------------|
| 200  | Number is divisible by 5      | {"value": "Buzz"}          |
| 200  | Number is not divisible by 5. | {"value": `<num>`}         |
| 400  | Number is out of bounds.      | {"error": "Out of bounds"} |

### GET /fizzbuzz/`<num>`

Returns the string value `Fizz` if `<num>` is divisible by 3, returns the string value `Buzz` if `<num>` is divisible by 5, and returns the string value `FizzBuzz` if the number is divisible by 3 and 5.  The only numbers that are acceptable are 1-500. Numbers outside of those bounds are rejected. The expected states with the return code and JSON responses are as follows:

| Code | State                              | JSON Response              |
|:----:|------------------------------------|----------------------------|
| 200  | Number is divisible by 3           | {"value": "Fizz"}          |
| 200  | Number is divisible by 5           | {"value": "Buzz"}          |
| 200  | Number is divisible by 3 and 5     | {"value": "FizzBuzz"}      |
| 200  | Number is not divisible by 3 and 5 | {"value": `<num>`}         |
| 400  | Number is out of bounds            | {"error": "Out of bounds"} |
